var google = require('googleapis');
var googleAuth = require('google-auth-library');
var config = require('config'); // модуль для подключения конфига
var async = require('async'); // модуль для подключения конфига
var Base64 = require('js-base64').Base64; // для шифрования почты в Base64

// Get config
var googleTokens = JSON.parse(JSON.stringify(config.get('googleTokens')));
var email = config.get('email');
var client_secret = JSON.parse(JSON.stringify(config.get('client_secret')));

var Thread = require('./models/Thread');
var Client = require('./models/Client');
var KeyWord = require('./models/KeyWords');
var Header = require('./models/Header');
var Ticket = require('./models/Ticket');
var Part = require('./models/Part');


// выбирает из массива значение из объекта, поле в котором name
var getHeaderInObjectWithValue = function(object, name) {
    for (var i =0; i<object.length; i++){
        if(object[i].name == name){
            return(object[i].value);
        }
    }
    return null;
};

// фильтрует их строки подстроку почты в скобках '<' '>'
var getFilterEmail = function (str) {
    if (str==null){
        return str;
    }
    var start = str.indexOf('<');
    var finish = str.indexOf('>');
    var length = finish-start-1;
    if (~str.indexOf('<')){
        var substr = str.substr(start+1, length);
        return substr;
    }else{
        return str;
    }
};

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    var clientSecret = credentials.web.client_secret;
    var clientId = credentials.web.client_id;
    var redirectUrl = 'http://localhost';
    var auth = new googleAuth();
    var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

    oauth2Client.credentials = googleTokens;
    callback(oauth2Client);
}


function listMessages(auth) {

    var date = new Date();
    date = date.getTime()-2*24*60*60*1000;
    date = new Date(date);
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    console.log("update after:"+year+"/"+(monthIndex+1)+'/'+day);
    var gmail = google.gmail('v1');
    gmail.users.messages.list({
        'auth' : auth,
        'userId': 'me',
        'q': "after:"+year+"/"+(monthIndex+1)+'/'+day
    }, function(err, response) {
        if (err) {
            console.log('The API returned an error: ' + err);
            return;
        }

        var messages = response.messages;

        if (messages)
            messages.forEach(function (item, i) {
                Ticket.findOne({idGmail : item.id}).populate('thread').exec(function(err, result){
                    if (err) return next(err);
                    if (!result) {
                        getMessage(auth, item.id);
                    }else{
                        // console.log('getMessage not run');
                    }
                });

            });
        // nextPageToken
        // resultSizeEstimate
    });
}

function getMessage(auth, idMessage) {
    var gmail = google.gmail('v1');
    gmail.users.messages.get({
            'auth' : auth,
            'userId': 'me',
            'id': idMessage
        },
        function(err, response) {
            if (err) {
                console.log('The API returned an error: ' + err);
                return;
            }

            async.waterfall([
                /**
                 * Находим поток, если есть - идем дальше, иначе создаем новый
                 */
                function(done) {
                    Thread.findOne({idThread : response.threadId}).exec(function(err, result){
                        if (err) return next(err);
                        var thread;
                        if (result){
                            thread = result;
                            thread.new = true;
                            done(err, thread, false);
                        }else{
                            var replyTo = getFilterEmail(getHeaderInObjectWithValue(response.payload.headers,'Reply-To'));
                            if (replyTo) {
                                thread = new Thread({
                                    idThread: response.threadId,
                                    status: 'Открыт',
                                    // question : true,
                                    replyTo: replyTo
                                });
                                thread.save(function (err) {
                                    done(err, thread, true);
                                });
                            }else{
                                // todo: если нет кому отвечать, отвечаем на ту почту, с которой пришел емейл
                                var from = getFilterEmail(getHeaderInObjectWithValue(response.payload.headers,'From'));
                                thread = new Thread({
                                    idThread: response.threadId,
                                    status: 'Открыт',
                                    // question : true,
                                    replyTo: from
                                });
                                thread.save(function (err) {
                                    done(err, thread, true);
                                });
                            }
                        }
                    });

                },
                /**
                 * Находим килента по почте, если он есть в базе
                 * @param thread поток
                 * @param newThread - параметр говорит о том что потом новый
                 */
                function (thread, newThread, done) {
                    // функция для того что бы найти клента

                    var replyTo = getFilterEmail(getHeaderInObjectWithValue(response.payload.headers,'Reply-To'));
                    if (newThread&&replyTo) {

                        Client.findOne({email: replyTo}).exec(function (err, result) {
                            if (err) return next(err);
                            // var thread;
                            if (result) {
                                thread.client = result;
                                thread.save(function (err) {
                                    done(err, thread, newThread);
                                });

                            }else{
                                done(err, thread, newThread);
                            };
                        });


                    }else{
                        done(err, thread, newThread);
                    }
                },
                function (thread, newThread, done) {
                    /**
                     * проверка на то, есть ли в объекте Subject ключевые поля для привязки к User
                     */
                    if (newThread) {
                        // console.log(getHeaderInObjectWithValue(response.payload.headers,'Subject'));

                        var subject = getHeaderInObjectWithValue(response.payload.headers,'Subject');
                        var index;
                        if (subject) {
                            KeyWord.find({}).exec(function (err, result) {
                                if (err) return next(err);
                                async.map(result, function iteratee(item, callback) {

                                    if (~subject.indexOf(item.word)) {
                                        index = item;
                                        // console.log('index');
                                        // console.log(index);
                                    }
                                    callback(null, item);

                                }, function done1(err) {
                                    if (index) {
                                        thread.user = index.user;
                                    }
                                    done(null, thread, newThread);
                                });
                            })
                        }

                    }else{
                        done(null, thread, newThread);
                    }

                },
                /**
                 * Создание тикета по письму
                 */
                function( thread, newThread, done) {
                    // console.log(thread);
                    var ticket = new Ticket({
                        // regular
                        name: '', // getSubjectInTicket(response)
                        idGmail: response.id,
                        idThread: response.threadId,
                        // user : //
                        snippet: response.snippet,
                        // payload fields
                        mimeType : response.payload.mimeType,
                        filename : response.payload.filename,
                        headers : [],
                        // body : response.payload.body,
                        parts : []//,
                        // thread : thread

                    });
                    // if (thread.client&&thread.client.email)

                    // не знаю под чем я был, но эта строчка в корне неверна
                    // var replayTo = thread.client&&thread.client.email;

                    var replayTo = getFilterEmail(getHeaderInObjectWithValue(response.payload.headers,'Reply-To'));
                    // console.log('replay to ',replayTo);
                    if (replayTo) {
                        if (~replayTo.indexOf(thread.replyTo)) {
                            // console.log('update ');
                            ticket.question = true;
                        }
                    }
                    // console.log('ticket');
                    // console.log(response.payload);
                    if (response.payload.body){
                        if (response.payload.body.data)
                            ticket.body = Base64.decode(response.payload.body.data);
                    }
                    // console.log(response.payload.headers);
                    if (response.payload.headers) {
                        Header.insertMany(response.payload.headers, function (err, result) {
                            ticket.headers = result;
                            done(null, thread, ticket, newThread);
                        });
                    }

                },
                function (thread, ticket, newThread, done) {
                    thread.lastTicket = ticket;
                    thread.lastDate =  ticket.createDate;

                    var replayTo = getFilterEmail(getHeaderInObjectWithValue(thread.lastTicket.headers,'Reply-To'));
                    // console.log('replay to ',replayTo);
                    if (replayTo&&!newThread) {
                        if (~replayTo.indexOf(thread.replyTo)) {
                            // console.log('открыт');
                            thread.status = 'Открыт';
                            // ticket.question = true;
                            // ticket.save(function (err) {
                            //     console.log('save ticket');
                            // });
                        }
                    }

                    thread.save(function (err) {
                        // console.log('err '+err);
                        // console.log(thread);
                        done(null, ticket);
                    });
                },
                function(ticket, done) {
                    // multipart/
                    // multipart/mixed
                    // multipart/alternative
                    if (~response.payload.mimeType.indexOf('multipart')){
                        async.map(response.payload.parts, function iteratee(item, callback) {
                            // сохранение parts
                            var part = new Part({
                                partId: item.partId,
                                mimeType: item.mimeType,
                                filename: item.filename
                            });

                            if (item.mimeType == 'text/html'){
                                ticket.body = Base64.decode(item.body.data);
                            }

                            async.waterfall([
                                    function(done) {
                                        if (item.body){
                                            if (item.body.data) {
                                                part.body = Base64.decode(item.body.data);
                                                done(null);
                                            }else{
                                                part.body = item.body.attachmentId;
                                                if (item.body.attachmentId) {
                                                    gmail.users.messages.attachments.get({
                                                        'auth' : auth,
                                                        'id': item.body.attachmentId,
                                                        'messageId': response.id,
                                                        'userId': 'me'
                                                    }, function(err, response) {
                                                        // console.log(response);
                                                        part.attachment= response.data;
                                                        part.size = response.size;
                                                        part.save(function(err) {
                                                            if (err)  console.log(err);
                                                            done(err);
                                                        });
                                                    });
                                                }else{
                                                    done(null);
                                                }
                                            }
                                        }else{
                                            done(null);
                                        }
                                    },
                                    function(done) {
                                        if (item.headers) {
                                            Header.insertMany(item.headers, function (err, result) {
                                                part.headers = result;
                                                part.save(function (err) {
                                                    if (err) return next(err);
                                                    done(null);
                                                });
                                            });
                                        }else{
                                            console.log('item.headers == undefined');
                                            done(null);
                                        }

                                    }
                                ],
                                function(err, ticket){
                                    if(err) {}
                                    async.setImmediate(function () {
                                        callback(null, part);
                                    });
                            });



                        }, function done1(err, result, res) {
                            ticket.parts = result;
                            done(null, ticket);

                        });
                    }else{
                        done(null, ticket);
                    }

                },
                function (ticket, done) {
                    if (!ticket.body){
                        ticket.body = ticket.snippet;
                    }
                    ticket.save(function (err) {
                        done(null, ticket);
                    });
                }
            ], function(err, ticket){
                if(err) {}
            });

        });

}


module.exports.getHeaderInObjectWithValue = getHeaderInObjectWithValue;
module.exports.getFilterEmail = getFilterEmail;
module.exports.authorize = authorize;
module.exports.listMessages = listMessages;
module.exports.getMessage = getMessage;
