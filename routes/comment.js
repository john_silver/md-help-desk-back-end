var express = require('express');
var async = require('async');

var app = express();



var Thread = require('../models/Thread');
var Comment = require('../models/Comment');

app.post('/api/comment/:id_thread', function (req, res, next) {
    async.waterfall([
        function(done) {
            Thread.findOne({ idThread: req.params.id_thread }).exec(function(err, result){
                if (err) return next(err);
                console.log(result);
                var thread = result;
                done(err, thread);

            });

        },
        function(thread, done) {
            var comment = new Comment({
                idThread : thread.idThread,
                text : req.body.text
            });
            comment.save(function (err) {
                done(err, comment, "comment created");
            });

        }
    ], function(err, comment, message){
        if(err) return next(err);
        res.status(200).send({ comment: comment, message: message, status : '200'});
    });
});

module.exports = app;
