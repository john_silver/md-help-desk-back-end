var express = require('express');
var async = require('async');

var app = express();


var Thread = require('../models/Thread');
var Ticket = require('../models/Ticket');
var Comment = require('../models/Comment');
var Part = require('../models/Part');

// лист;
app.get('/api/thread', function (req, res, next) {
    /**
     *
     * @params:
     * userId
     * status
     * filterByOwner
     */
    var filter = {};
    var status = req.query.status;
    var filterByOwner = req.query.filterByOwner;
    var userId = req.query.userId;
    if (status){filter.status = status;}
    if (filterByOwner=='withMe'){ // если надо проверить связь

    }
    if(filterByOwner=='my'){
        filter.user = userId;
    }
    if (filterByOwner=='all') {

    }
    console.log(filter.user);

    /**
     * filterByOwner:
     *  me
     *  withMe
     *  all
     */
    var perPage = 10;
    var page = (req.query.page==undefined)?(1):(req.query.page);

    Thread.count(filter, function(err, count) {
        var countThreads = count;
        Thread.find(filter)
            .skip(perPage * (page - 1))
            .limit(perPage)
            .sort({
                'new':-1,
                id :-1
            })
            .deepPopulate('lastTicket.headers user client').exec(function(err, result){
            if (err) return next(err);
            res.status(200).send({ tickets: result, count: countThreads, page: page, countPage: (countThreads%perPage)+1,  message: 'Success'});
        });
    });


});


// list detail
app.get('/api/thread/:id_thread', function (req, res, next) {
    console.log('/api/thread/:id_thread');

    async.waterfall([
        function( done) {
            // console.log(req.params.id_ticket);

            Ticket.find({idThread: req.params.id_thread}).populate('headers parts').exec(function(err, result){
                if (err) return next(err);
                // console.log(result);

                done(null, result, req.params.id_thread);
            });

        },
        function (tickets, idThread, done) {
            Thread.findOne({idThread : idThread}).populate('user client').exec(function(err, thread){
                if (err) return next(err);
                thread.new = false;
                thread.save();
                done(err, tickets, thread, idThread);
            });
        },
        function (tickets, thread, idThread, done) {
            Comment.find({idThread : idThread}).exec(function(err, comments){
                if (err) return next(err);
                console.log(comments);
                done(err, tickets, thread, comments);
            });
        }
    ], function(err, tickets, thread, comments){
        if(err) return next(err);
        res.status(200).send({ tickets: tickets, thread: thread, comments:comments, message: 'Success'});
    });
});


app.get('/api/thread/user/:id_client', function (req, res, next) {

    async.waterfall([
        function(done) {
            Thread.find({client: req.params.id_client}).populate('user client lastTicket').deepPopulate('lastTicket.headers').exec(function(err, result){
                if (err) return next(err);
                // console.log(result);
                done(null, result);
            });
        }
    ], function(err, threads){
        if(err) return next(err);
        res.status(200).send({ threads: threads, message: 'Success'});
    });


});

app.post('/api/thread/:id_thread', function (req, res, next) {

    Thread.findOne({idThread: req.params.id_thread}).exec(function(err, result){
        if (err) return next(err);
        result.status = req.body.status;
        result.save(function (err) {
            if(err) return next(err);
            res.status(200).send({ message: 'Success'});
        })

    });

});


module.exports = app;
