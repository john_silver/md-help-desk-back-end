var express = require('express');
var async = require('async');
var app = express();
var config = require('config'); // модуль для подключения конфига
var google = require('googleapis');
var Base64 = require('js-base64').Base64;
var nodemailer = require('nodemailer');

// Get config
var googleTokens = JSON.parse(JSON.stringify(config.get('googleTokens')));
var email = config.get('email');
var client_secret = JSON.parse(JSON.stringify(config.get('client_secret')));

var getHeaderInObjectWithValue = require('../help').getHeaderInObjectWithValue;
var authorize = require('../help').authorize;
var listMessages = require('../help').listMessages;

var Thread = require('../models/Thread');
var Ticket = require('../models/Ticket');
var Comment = require('../models/Comment');

app.get('/api/ticket', function (req, res, next) {

    Ticket.find({}).populate('headers').exec(function(err, result){
        if (err) return next(err);
        res.status(200).send({ tickets: result, message: 'Success'});
    });

});
app.get('/api/ticket/count', function (req, res, next) {
    Ticket.count({}, function( err, count){
        if (err) return next(err);
        console.log( "Number of users:", count);
        res.status(200).send({count: count, status : '200', message : 'OK'});
    });
});

app.get('/api/ticket/:id_ticket', function (req, res, next) {

    async.waterfall([
        function(done) {
            Ticket.findById(req.params.id_ticket, function(err, ticket) {
                if (err) return next(err);
                done(null, ticket);
            });
        },
        function(ticket, done) {
            if (!ticket) {
                done(true);
                return;
            }
            // console.log(req.params.id_ticket);

            Ticket.find({idThread: ticket.idThread}).populate('headers parts').exec(function(err, result){
                if (err) return next(err);
                // console.log(result);
                done(null, result, ticket.idThread);
            });

        },
        function (tickets, idThread, done) {
            Thread.findOne({idThread : idThread}).populate('user client').exec(function(err, thread){
                if (err) return next(err);
                done(err, tickets, thread, idThread);
            });
        },
        function (tickets, thread, idThread, done) {
            console.log('Comments');
            console.log(idThread);
            Comment.find({idThread : idThread}).exec(function(err, comments){
                if (err) return next(err);
                console.log(comments);
                done(err, tickets, thread, comments);
            });
        }
    ], function(err, tickets, thread, comments){
        if(err) return next(err);
        res.status(200).send({ tickets: tickets, thread: thread, comments:comments, message: 'Success'});
    });

});


function sendEmail(item, callback) {
    var html = '';

    html = '<div>' +
        '<div style="text-align: center;">' +
        '<img src="http://invoice.mirusdesk.kz/images/logo.png"><br><br><br>' +
        '</div><br><br>'+

        item.body+


        '<div style="color : #666666;">MIRUSDESK - Прогрессивные бухгалтерские решения<br>'+
        'тел.: +7 (727) 315-2152, моб.: +7 (701) 981-7860 <br>'+
        '<a href="http://www.mirusdesk.kz/" target="_blank" style="color : #666666;"> www.mirusdesk.kz</a> | <a href="mailto:support@mirusdesk.kz" target="_blank">support@<wbr>mirusdesk.kz</a>'+
        '</div></div>';

    // ticket@mirusdesk.kz
    // F$VXX8yE9cgj%Ww3

    var attach = [];
    if (item.attach){
        item.attach.forEach(function (item) {
            var file = {
                filename: item.filename,
                content: item.base64,
                encoding: 'base64'
            };
            attach.push(file);
        })
    }

    // 'smtp://ticket@mirusdesk.kz:' + encodeURIComponent("F$VXX8yE9cgj%Ww3") +'@smtp.gmail.com'
    // var transporter = nodemailer.createTransport('smtps://ticket@mirusdesk.kz:' + encodeURIComponent("F$VXX8yE9cgj%Ww3") +'@smtp.gmail.com');

    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        auth: {
            user: 'ticket@mirusdesk.kz',
            pass: 'F$VXX8yE9cgj%Ww3'
        }
    });

    // var transporter = nodemailer.createTransport('smtps://ticket@mirusdesk.kz:F$VXX8yE9cgj%Ww3@smtp.gmail.com');
    // var transporter = nodemailer.createTransport('smtps://support@mirusdesk.kz:Z123456789z@smtp.gmail.com');

// setup e-mail data with unicode symbols
    var mailOptions = {
        from: 'ticket@mirusdesk.kz', // sender address
        replyTo :'ticket@mirusdesk.kz',
        to: item.to, // list of receivers
        // 'ticket@mirusdesk.kz'
        subject: item.subject, // Subject line
        // text: 'Hello world 🐴',
        html: html,
        attachments: attach
    };

// send mail with defined transport object
    console.log(mailOptions);
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
            // console.log(item.email);
            // sendEmail(item);
            callback();
            return console.log(error);
        }
        callback();
        console.log('Message sent: ' + info.response);

    });
}

app.post('/api/ticket/sendMail', function (req, res, next) {
    sendEmail({
        to : req.body.to,
        body : req.body.body,
        subject : req.body.subject,
        attach : req.body.attachments
    }, function (err) {
        res.status(200).send({ message: 'Success'});
    });
});

app.post('/api/ticket/:id_thread/reply', function (req, res, next) {
    console.log('reply');



    function makeBody(to, from, subject, message, references, replyTo, attachments) {
        var nl = "\n";
        var boundary = "__automato__";
        var boundary2 = "__automato2__";

        var str = [
            "MIME-Version: 1.0",
            "In-Reply-To: "+replyTo, //Message-Id
            "References: "+references,
            "Subject: "+'=?utf-8?B?'+ Base64.encode(subject)+'?=',
            "To: "+ to,
            "From: "+ from,


            "Content-Type: multipart/mixed; boundary=" + boundary + nl,
            "--" + boundary,

            "Content-Type: multipart/alternative; boundary=" + boundary2 + nl,
            "--" + boundary,

            "Content-Type: text/plain; charset=\"UTF-8\"",
            "Content-Transfer-Encoding: base64",
            Base64.encode(message) + nl,
            "--" + boundary2,

            "Content-Type: text/html; charset=UTF-8",
            "Content-Transfer-Encoding: base64",
            "Content-Disposition: inline",
            Base64.encode(message) + nl,

            "--" + boundary2 + "--"

        ];

        for (var i = 0; i < attachments.length; i++) {
            var attachment = [
                "--" + boundary,
                "Content-Type: " + attachments[i].filetype + '; name="' + attachments[i].filename + '"',
                'Content-Disposition: attachment; filename="' + attachments[i].filename + '"',
                "Content-Transfer-Encoding: base64" + nl,
                attachments[i].base64
            ];

            str.push(attachment.join(nl));

        }
        str.push("--" + boundary + "--");
        str  = str.join(nl);
        console.log(str);
        var encodedMail = new Buffer(str).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');
        // console.log(encodedMail);
        return encodedMail;
    }

    // function makeBody(to, from, subject, message, references, replyTo) {
    //     var str = ["Content-Type: text/plain; charset=\"UTF-8\"\n",
    //         "MIME-Version: 1.0\n",
    //         "Content-Transfer-Encoding: UTF-8\n",
    //         "In-Reply-To: "+replyTo+"\n", //Message-Id
    //         "References: "+references+"\n",
    //         "Subject: ", '=?utf-8?B?', Base64.encode(subject),'?=',"\n",
    //         "to: ", to, "\n",
    //         "from: ", from, "\n",
    //         "\n",
    //         message
    //     ].join('');
    //     var encodedMail = new Buffer(str).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');
    //     return encodedMail;
    // }

    function sendMessage(auth, obj, threadId, thread, done) {
        console.log('sendMessage');
        console.log(obj);
        console.log(threadId);
        var raw = makeBody(obj.to, obj.from, obj.subject, obj.message, obj.references, obj.replyTo, obj.attachments);
        var gmail = google.gmail('v1');
        gmail.users.messages.send({
            auth: auth,
            userId: 'me',
            resource: {
                raw: raw,
                threadId : threadId
            }
        }, function(err, response) {
            console.log('sendMessage 2');
            console.log(err);
            console.log(response);
            authorize(client_secret, listMessages);
            done(null, thread);
        });
    }


    console.log(req.params.id_thread);

    async.waterfall([
        function(done) {
            Thread.findOne({idThread: req.params.id_thread}).deepPopulate('lastTicket.headers').exec(function(err, thread) {
                if (err) return next(err);
                console.log('adsdsd');
                console.log(thread.lastTicket);
                done(null, thread);

            });

        },
        function( thread, done) {

            // console.log(getHeaderInObjectWithValue(thread.lastTicket.headers,'Reply-To'));
            // console.log(getHeaderInObjectWithValue(thread.lastTicket.headers,'Message-ID'));
            // In-Reply-To -> Message-ID
            // References -> References + Message-ID
            var references = getHeaderInObjectWithValue(thread.lastTicket.headers,'References');
            if (references==null){
                references = '';
            }
            authorize( client_secret, function (auth) {
                sendMessage(auth, {
                    to: thread.replyTo,
                    from : email,
                    subject : getHeaderInObjectWithValue(thread.lastTicket.headers,'Subject'),
                    message: req.body.body,
                    references: references + ' ' + getHeaderInObjectWithValue(thread.lastTicket.headers,'Message-ID'),
                    replyTo: getHeaderInObjectWithValue(thread.lastTicket.headers,'Message-ID'),
                    attachments: req.body.attachments
                }, thread.idThread, thread, done)
            } );
        },
        function (thread, done) {
            thread.status = "Ожидание";
            thread.save(function (err) {
                done(null, thread);
            });
        }
    ], function(err, thread){
        if(err) return next(err);
        res.status(200).send({ thread: thread, message: 'Success'});
    });


});




module.exports = app;
