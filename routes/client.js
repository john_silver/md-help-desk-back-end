var express = require('express');
var async = require('async');
var app = express();


var Client = require('../models/Client');

var mongoose = require('mongoose');

var connect = mongoose.createConnection('mongodb://localhost/ticket');


// stored in 'testA' database
var TicketUser    = connect.model('User', new mongoose.Schema({
    name: String,
    // email: { type: String, unique: true, lowercase: true, trim: true },
    password: String,
    idToEnter: String,
    statusCompany: String,
    nameCompany: String,
    representative: String,
    telephone: String,
    email: String
}));


// Добавление
app.put('/api/client', function(req, res, next) {
    async.waterfall([
        function( done) {
            var object = {
                // regular
                name: req.body.name,
                password: req.body.idToEnter,
                idToEnter: req.body.idToEnter,
                telephone: req.body.telephone,
                email: req.body.email,
                role: 0,
                // поля клиента
                statusCompany: req.body.statusCompany,
                nameCompany: req.body.nameCompany,
                representative: req.body.representative

            };
            var client = new Client(object);
            client.save(function(err) {
                if (err) return next(err);

                // Добавляем во творую базу юзера
                object.password = client.password;
                var ticketUser = new TicketUser(object);
                ticketUser.save(function(err) {
                    if (err) return next(err);
                    console.log('пользователь тикета сохранен');
                    // console.log(ticketUser);
                    res.status(200).send({ client: client, message: 'Success'});
                });


            });

        }
    ])

});

// изменение
app.post('/api/client/:id', function(req, res, next) {
    async.waterfall([
        function (done) {
            Client.findById(req.params.id, function(err, client) {
                if (err) return next(err);
                done(null, client);
            });
        },
        function (client, done) {
            TicketUser.findOne({idToEnter : client.idToEnter}, function(err, ticketUser) {
                if (err) return next(err);
                done(null, client, ticketUser);
            });
        },
        function(client, ticketUser, done) {
            client.name = req.body.name;
            // client.password = req.body.idToEnter;
            client.idToEnter = req.body.idToEnter;
            client.telephone = req.body.telephone;
            client.email = req.body.email;
            // client.role = req.body.role;

            client.statusCompany = req.body.statusCompany;
            client.nameCompany = req.body.nameCompany;
            client.representative = req.body.representative;

            ticketUser.name = req.body.name;
            ticketUser.idToEnter = req.body.idToEnter;
            ticketUser.telephone = req.body.telephone;
            ticketUser.email = req.body.email;
            ticketUser.statusCompany = req.body.statusCompany;
            ticketUser.nameCompany = req.body.nameCompany;
            ticketUser.representative = req.body.representative;

            ticketUser.save(function(err) {
                console.log("Пользователь тикета обновлен");
            });

            client.save(function(err) {
                if (err) return next(err);
                done(err, client);
            });
        }
    ], function(err, client){
        if(err) return next(err);
        res.status(200).send({ client: client, message: 'Success'});
    });

});

app.get('/api/client/:id', function (req, res, next) {

    Client.findById(req.params.id, function(err, user) {
        if (err) return next(err);
        res.status(200).send({ client: user, message: 'Success'});
    });

});


// список
app.get('/api/client', function (req, res, next) {

    Client.find({}).exec(function(err, result){
        if (err) return next(err);
        res.status(200).send({ clients: result, message: 'Success'});
    });

});


module.exports = app;
