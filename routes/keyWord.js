var express = require('express');
var async = require('async');
var app = express();


var KeyWord = require('../models/KeyWords');


// Добавление
app.put('/api/keyword', function(req, res, next) {
    async.waterfall([
        function( done) {
            var key = new KeyWord({
                user : req.body.user,
                word : req.body.word
            });
            key.save(function(err) {
                if (err) return next(err);
                res.status(200).send({ keyword: key, message: 'Success'});
            });
        }
    ])

});


// изменение
app.post('/api/keyword/:id', function(req, res, next) {
    async.waterfall([
        function (done) {
            KeyWord.findById(req.params.id, function(err, key) {
                if (err) return next(err);
                done(null, key);
            });
        },
        function(key, done) {
            key.user = req.body.user;
            key.word = req.body.word;

            key.save(function(err) {
                if (err) return next(err);
                done(err, key);
            });
        }
    ], function(err, key){
        if(err) return next(err);
        res.status(200).send({ keyword: key, message: 'Success'});
    });

});

// список
app.get('/api/keyword/:id_user', function (req, res, next) {

    KeyWord.find({ user: req.params.id_user }).exec(function(err, result){
        if (err) return next(err);
        res.status(200).send({ keywords: result, message: 'Success'});
    });

});

// удаление
app.delete('/api/keyword/:id', function (req, res, next) {

    KeyWord.remove({ _id: req.params.id }, function(err) {
        if (err) return next(err);
        res.status(200).send({ result: err, message: 'Success'});
    });

});




module.exports = app;
