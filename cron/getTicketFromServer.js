var cron = require('cron');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var config = require('config'); // модуль для подключения конфига



var getHeaderInObjectWithValue = require('../help').getHeaderInObjectWithValue;
var getFilterEmail = require('../help').getFilterEmail;
var authorize = require('../help').authorize;
var listMessages = require('../help').listMessages;
var getMessage = require('../help').getMessage;



// Get config
var googleTokens = JSON.parse(JSON.stringify(config.get('googleTokens')));
var email = config.get('email');
console.log(email);
var client_secret = JSON.parse(JSON.stringify(config.get('client_secret')));
console.log(googleTokens);



// ticket id 15886342994c6174

authorize(client_secret, listMessages);
var cronJob = cron.job("*/60 * * * * *", function(){
    // perform operation e.g. GET request http.get() etc.
    console.log('get list from server');
    authorize(client_secret, listMessages);
});
cronJob.start();