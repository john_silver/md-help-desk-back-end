var cron = require('cron');
var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/gmail-nodejs-quickstart.json
var SCOPES = [
    'https://mail.google.com/',
    'https://www.googleapis.com/auth/gmail.modify',
    'https://www.googleapis.com/auth/gmail.compose',
    'https://www.googleapis.com/auth/gmail.readonly',
    'https://www.googleapis.com/auth/gmail.send'];
// 'https://mail.google.com/',
//     'https://www.googleapis.com/auth/gmail.modify',
//     'https://www.googleapis.com/auth/gmail.compose',
//     'https://www.googleapis.com/auth/gmail.send'

var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'gmail-nodejs-quickstart.json';
console.log(TOKEN_DIR);

var googleTokens = {
    "access_token":"ya29.Ci-HA0vFg3t8naBWut2uIizAraLVFgf_483cLaR_YtDYBjzVZlZMgy2LtYfueSLiNw",
    "refresh_token":"1/e0GDgFIrN8qVcbvuop12_mcUVKkrURF7n3Db4qRdqG0",
    "token_type":"Bearer",
    "expiry_date":1477400374924};

var client_secret = {"web":
    {
        "client_id":"185296150255-5tktu04vilro2fnp86622t4rbqn770mg.apps.googleusercontent.com",
        "project_id":"ticket-147413",
        "auth_uri":"https://accounts.google.com/o/oauth2/auth",
        "token_uri":"https://accounts.google.com/o/oauth2/token",
        "auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs",
        "client_secret":"NCjs33EiGC8u3xLD0JFu4r-n"
    }
}

// Load client secrets from a local file.
fs.readFile('client_secret.json', function processClientSecrets(err, content) {
    if (err) {
        console.log('Error loading client secret file: ' + err);
        return;
    }
    // Authorize a client with the loaded credentials, then call the
    // Gmail API.
    authorize(JSON.parse(content), sendMessage);
    // listMessages()
    // listLabels
    // getMessage
});

// Для этого в письмо-ответ надо добавить заголовок In-Reply-To с
// указанием значения заголовка Message-Id оригинального письма.
// В заголовок References ответа надо скопировать все ID из заголовка
// References оригинала и дописать после пробела значение заголовка Message-Id
// оригинального письма.

var Base64 = require('js-base64').Base64;
// var quotedPrintable = require('quoted-printable');
// var utf8 = require('utf8');
var mailcomposer = require("mailcomposer");
// console.log(utf8.decode('Тема письма'));

// var mailOptions = {
//     from: 'johnsilver7311@gmail.com', // sender address
//     replyTo : 'johnsilver7311@gmail.com',
//     to: 'johnsilver7311@gmail.com', // list of receivers
//     subject: '"Заявка ', // Subject line
//     // text: 'Hello world 🐴', // plaintext body
//     html: 'asdasdasdaолвлфырвфлыов' // html body
// };
// var mail = mailcomposer(mailOptions);
// console.log(mail);
// return ;


function makeBody(to, from, subject, message) {
    var str = ["Content-Type: text/plain; charset=\"UTF-8\"\n",
        "MIME-Version: 1.0\n",
        "Content-Transfer-Encoding: UTF-8\n",
        "In-Reply-To: <1477723595.323253979@f83.i.mail.ru>\n", //Message-Id
        "References: <1477723595.323253979@f83.i.mail.ru>\n",
        "Subject: ", 'test',"\n",
        // "Subject: ", '=?utf-8?B?',Base64.encode(subject),'?=',"\n",
        "to: ", to, "\n",
        "from: ", from, "\n",
        "\n",
        message
    ].join('');
    // console.log(str);
    // quotedPrintable.encode(utf8.encode(subject))
    var encodedMail = new Buffer(str).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');
    console.log(encodedMail);
    // var encodedMail = Base64.encodeURI(str);
    return encodedMail;
}

function sendMessage(auth) {
    var raw = makeBody('aleckseypro@mail.ru', 'johnsilver7311@gmail.com', 'Тема письма', 'Тема письма');
    var gmail = google.gmail('v1');
    gmail.users.messages.send({
        auth: auth,
        userId: 'me',

        resource: {
            raw: raw,
            threadId : '1580f2440a1f0a69'
        }
    }, function(err, response) {
        console.log(err || response)
    });
}

var cronJob = cron.job("*/5 * * * * *", function(){
    // perform operation e.g. GET request http.get() etc.
    console.info('cron job completed');
});
// cronJob.start();

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
    var clientSecret = credentials.web.client_secret;
    var clientId = credentials.web.client_id;
    var redirectUrl = 'http://localhost';
    var auth = new googleAuth();
    console.log(credentials);
    var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);
    getNewToken(oauth2Client, callback);
    // Check if we have previously stored a token.
    // fs.readFile(TOKEN_PATH, function(err, token) {
    //     if (err) {
    //         getNewToken(oauth2Client, callback);
    //     } else {
    //         oauth2Client.credentials = JSON.parse(token);
    //         callback(oauth2Client);
    //     }
    // });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
    var authUrl = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    rl.question('Enter the code from that page here: ', function(code) {
        rl.close();
        oauth2Client.getToken(code, function(err, token) {
            if (err) {
                console.log('Error while trying to retrieve access token', err);
                return;
            }
            oauth2Client.credentials = token;
            storeToken(token);
            callback(oauth2Client);
        });
    });
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
    try {
        fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
        if (err.code != 'EEXIST') {
            throw err;
        }
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token));
    console.log('Token stored to ' + TOKEN_PATH);
}

/**
 * Lists the labels in the user's account.
 *
 * @param {google.auth.OAuth2} auth An authorized OAuth2 client.
 */
function listLabels(auth) {
    var gmail = google.gmail('v1');
    gmail.users.labels.list({
        auth: auth,
        userId: 'me'
    }, function(err, response) {
        if (err) {
            console.log('The API returned an error: ' + err);
            return;
        }
        console.log(response);
        var labels = response.labels;
        if (labels.length == 0) {
            console.log('No labels found.');
        } else {
            console.log('Labels:');
            for (var i = 0; i < labels.length; i++) {
                var label = labels[i];
                console.log('- %s', label.name);
            }
        }
    });
}


/**
 * Retrieve Messages in user's mailbox matching query.
 *
 * @param  {String} userId User's email address. The special value 'me'
 * can be used to indicate the authenticated user.
 * @param  {String} query String used to filter the Messages listed.
 * @param  {Function} callback Function to call when the request is complete.
 */
function listMessages(auth) {
    // userId, query, callback
    // var getPageOfMessages = function(request, result) {
    //     request.execute(function(resp) {
    //         result = result.concat(resp.messages);
    //         var nextPageToken = resp.nextPageToken;
    //         if (nextPageToken) {
    //             var gmail = google.gmail('v1');
    //             request = gmail.users.messages.list({
    //                 'auth' : auth,
    //                 'userId': 'me',
    //                 'pageToken': nextPageToken,
    //                 'q': query
    //             });
    //             getPageOfMessages(request, result);
    //         } else {
    //             callback(result);
    //         }
    //     });
    // };
    // var gmail = google.gmail('v1');
    // var initialRequest = gmail.users.messages.list({
    //     'auth' : auth,
    //     'userId': 'me',
    //     'q': ''
    // });
    // getPageOfMessages(initialRequest, []);
    console.log('request sended');

    var gmail = google.gmail('v1');
    gmail.users.messages.list({
        'auth' : auth,
        'userId': 'me',
    }, function(err, response) {
        if (err) {
            console.log('The API returned an error: ' + err);
            return;
        }
        console.log(response);
        // var labels = response.labels;
        // if (labels.length == 0) {
        //     console.log('No labels found.');
        // } else {
        //     console.log('Labels:');
        //     for (var i = 0; i < labels.length; i++) {
        //         var label = labels[i];
        //         console.log('- %s', label.name);
        //     }
        // }
    });


}

function getMessage(auth) {
    console.log('request sended');
    // gmail.users.messages.get({
    //     'userId': userId,
    //     'id': messageId
    // });
    var gmail = google.gmail('v1');
    gmail.users.messages.get({
        'auth' : auth,
        'userId': 'me',
        'id': '158098a668ddd588'
        // 158098a668ddd588
        // 15805b7b1408f6b7
    }, function(err, response) {
        if (err) {
            console.log('The API returned an error: ' + err);
            return;
        }
        console.log(response);
        // console.log(new Buffer(response.payload.body.data, 'base64').toString('utf8'));


    });


}

