var compression = require('compression');
var express = require('express');

var path = require('path');
var logger = require('morgan');
var http = require('http');
var cors = require('cors');

var mongoose = require('mongoose');
require('mongoose-double')(mongoose);

/*var fs = require('fs');*/

// var multer  = require('multer');
// var upload = multer({ dest: 'uploads/' });

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var async = require('async');

var session = require('express-session');
var MongoStore = require('connect-mongo')(session);




// cron function
require('./cron/closeTicket');
require('./cron/getTicketFromServer');


mongoose.connect('mongodb://127.0.0.1:27017/ticketPlan');

var app = express();

// Middlewares
app.use(cors());
app.set('port', process.env.PORT || 8081); // для help desk
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use(session({ secret: 'some key',
    resave:  true,
    saveUninitialized: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection })
}));

// Use of Passport user after login
app.use(function(req, res, next) {
    if (req.user) {
        res.cookie('user', JSON.stringify(req.user));
    }
    next();
});

app.use(require('./routes/help.js'));
app.use(require('./routes/auth.js'));
app.use(require('./routes/thread.js'));
app.use(require('./routes/comment.js'));
app.use(require('./routes/ticket.js'));

app.use(require('./routes/user.js'));
app.use(require('./routes/client.js'));
app.use(require('./routes/keyWord.js'));
app.use(require('./routes/attachment.js'));


app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.send(500, { message: err.message });
});

// Start server
var server = app.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});
