var mongoose = require('mongoose');
var deepPopulate = require('mongoose-deep-populate')(mongoose);
var autoIncrement = require('mongoose-auto-increment');

autoIncrement.initialize(mongoose.createConnection('mongodb://127.0.0.1:27017/ticketPlan'));

var threadSchema = new mongoose.Schema({
    // regular
    createDate: { type: Date, default: Date.now },
    id : Number,
    idThread : String,
    replyTo : String,
    status : String,
    star : { type: Boolean, default: false },
    user : { type: mongoose.Schema.ObjectId, ref: 'User' },
    lastTicket : { type: mongoose.Schema.ObjectId, ref: 'Ticket' },
    client : { type: mongoose.Schema.ObjectId, ref: 'Client' },
    lastDate : Date,
    'new':{ type: Boolean, default: true },
});
threadSchema.plugin(deepPopulate);
threadSchema.plugin(autoIncrement.plugin, {
    model: 'Thread',
    field: 'id',
    startAt: 1

});
module.exports = mongoose.model('Thread', threadSchema);