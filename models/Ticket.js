var mongoose = require('mongoose');

// user schema
var ticketSchema = new mongoose.Schema({
    // regular
    status: String,
    name : String, // Subject in mail
    idGmail : { type: String, unique: true },
    idThread : String,
    user : { type: mongoose.Schema.ObjectId, ref: 'User' },
    snippet : String,
    // payload : {type: mongoose.Schema.ObjectId, ref : 'Payload'}

    // payload fields
    mimeType: String,
    filename: String,
    headers: [{type: mongoose.Schema.ObjectId, ref : 'Header'}],
    body : String,
    parts: [{type : mongoose.Schema.ObjectId, ref : 'Part'}],
    createDate: { type: Date, default: Date.now },
    // thread : {type: mongoose.Schema.ObjectId, ref : 'Thread'}

    question : { type: Boolean, default: false }

});



module.exports = mongoose.model('Ticket', ticketSchema);