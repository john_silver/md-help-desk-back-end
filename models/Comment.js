var mongoose = require('mongoose');

var commentSchema = new mongoose.Schema({

    type : { type: String, default: "Comment" },
    idThread : String,
    text : String,
    createDate: { type: Date, default: Date.now }

});



module.exports = mongoose.model('Comment', commentSchema);