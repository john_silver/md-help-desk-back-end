var mongoose = require('mongoose');

// user schema
var partSchema = new mongoose.Schema({
    // regular
    partId: String,
    mimeType: String,
    filename: String,
    headers: [{type: mongoose.Schema.ObjectId, ref : 'Header'}],
    // added other field
    body : String,
    attachment : String,
    size : Number
});



module.exports = mongoose.model('Part', partSchema);