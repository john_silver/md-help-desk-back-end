var mongoose = require('mongoose');

// user schema
var payloadSchema = new mongoose.Schema({
    // regular
    mimeType: String,
    filename: String,
    headers: [{type: mongoose.Schema.ObjectId, ref : 'Headers'}],
    // added other field
    body : String,
    parts: [{type : mongoose.Schema.ObjectId, ref : 'Part'}]
});



module.exports = mongoose.model('Payload', payloadSchema);