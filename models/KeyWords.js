var mongoose = require('mongoose');

// user schema
var keyWordSchema = new mongoose.Schema({

    word : String, // Subject in mail
    user : { type: mongoose.Schema.ObjectId, ref: 'User' },
    createDate: { type: Date, default: Date.now }

});

// Счет-фактура
// Счет на оплату
// Накладная
// Акт выполненных работ
// Поступление
// Платеж
// Справка
// Прием на работу
// Отпуск
// Увольнение с работы
// Акт сверки
// Справка об отсутствии задолженности
// Доверенность

module.exports = mongoose.model('KeyWord', keyWordSchema);